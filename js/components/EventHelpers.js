export const WORKING_GROUPS = [
  {
    id: "eclipse_org",
    name: "Eclipse.org"
  },
  {
    id: "ecd_tools",
    name: "Eclipse Cloud Development Tools"
  },
  {
    id: "edge_native",
    name: "Eclipse Edge Native"
  },
  {
    id: "eclipse_iot",
    name: "Eclipse IoT"
  },
  {
    id: "jakarta_ee",
    name: "Jakarta EE"
  },
  {
    id: "openadx",
    name: "OpenADx"
  },
  {
    id: "opengenesis",
    name: "OpenGENESIS"
  },
  {
    id: "openhwgroup",
    name: "OpenHW Group"
  },
  {
    id: "openmdm",
    name: "OpenMDM"
  },
  {
    id: "openmobility",
    name: "OpenMobility"
  },
  {
    id: "openpass",
    name: "OpenPASS"
  },
  {
    id: "osgi",
    name: "OSGI"
  },
  {
    id: "science",
    name: "Science"
  },
  {
    id: "sparkplug",
    name: "Eclipse Sparkplug"
  },
  {
    id: "tangle_ee",
    name: "Tangle EE"
  },
  {
    id: "eclipse_ide",
    name: "Eclipse IDE"
  },
  {
    id: "asciidoc",
    name: "AsciiDoc"
  },
  {
    id: "research",
    name: "Eclipse Research"
  },
  {
    id: "ospo_zone",
    name: "OSPO Zone"
  },
  {
    id: "oniro",
    name: "Oniro Project"
  }
]

export const EVENT_TYPES = [
  {
    id: "conference",
    name: "Conference"
  },
  {
    id: "dc",
    name: "Demo Camps & Stammtisch"
  },
  {
    id: "ec",
    name: "EclipseCon"
  },
  {
    id: "et",
    name: "Training Series"
  },
  {
    id: "webinar",
    name: "Webinar"
  },
  {
    id: "wg",
    name: "Working Group Events"
  },
]

export const EVENT_ATTENDANCE_TYPE = [
  {
    id: "virtual",
    name: "Virtual",
  },
  {
    id: "in_person",
    name: "In Person",
  },
  {
    id: "hybrid",
    name: "Hybrid",
  },
]

export const EVENT_PARTICIPATION_TYPE = [
  {
    id: "ef_event",
    name: "Eclipse Foundation Events"
  },
  {
    id: "ef_attending",
    name: "Events we are attending"
  },
  {
    id: "member_org_participating",
    name: "Member organization participating"
  },
  {
    id: "other",
    name: "Other community events of interest"
  }
]

export function getSelectedItems(checkedItems) {
  let selected = []
  if (checkedItems) {
    for (const property in checkedItems) {
      if (checkedItems[property]) {
        selected.push(property)
      }
    }
    return selected
  }
}

export function hasAddress(event) {
  if (event.address && (event.address.city || event.address.country)) {
    return true
  } else return false
}

export function checkSameMonth(startDate, endDate) {
  return startDate.getMonth() === endDate.getMonth()
}

export function checkSameDay(startDate, endDate) {
  return checkSameMonth(startDate, endDate) && startDate.getDate() === endDate.getDate()
}

export function generateDate(date, locale) {
  if (date) {
    return date.toLocaleDateString(locale, { weekday: 'short', month: 'short', day: 'numeric' })
  }
}

export function generateDates(startDate, endDate, locale = undefined) {
  if (endDate && !checkSameDay(startDate, endDate)) {
    return generateDate(startDate, locale) + " - " + generateDate(endDate, locale) + ", " + startDate.getFullYear()
  }
  else {
    return generateDate(startDate, locale)
  }
}

export function generateTime(time, locale) {
  if (time) {
    return time.toLocaleTimeString(locale, { hour: '2-digit', minute: '2-digit' })
  }
}

export function generateTimes(startDate, endDate, locale = []) {
  if (endDate && checkSameDay(startDate, endDate)) {
    return generateTime(startDate, locale) + " - " + generateTime(endDate, locale)
  }
  else {
    return generateTime(startDate, locale)
  }
}

export function checkDatePast(inputDate) {
  var today = new Date();
  var input_date = new Date(inputDate);
  if (today < input_date) {
    return false;
  } else return true
}

export function alphaOrder(array) {

  if (array) {
    return array.sort((a, b) => a?.name.localeCompare(b?.name))
  }

}

export function hasSelectedItems(items) {
  let selectedItems = getSelectedItems(items)
  if (selectedItems && selectedItems.length > 0) {
    return selectedItems
  } else return false
}


export function getUrl(page, searchParas, groupParas, typeParas, attendanceParas, participationParas) {
  let url = `https://newsroom.eclipse.org/api/events?&page=${page}&pagesize=10&options[orderby][field_event_date]=custom`;
  
  groupParas && groupParas.forEach((item) => (url = url + '&parameters[publish_to][]=' + item));
  typeParas && typeParas.forEach((item) => (url = url + '&parameters[type][]=' + item));
  participationParas && participationParas.forEach((item) => (url = url + '&parameters[ef_participation][]=' + item));
  attendanceParas && attendanceParas.forEach((item) => (url = url + '&parameters[attendance_type][]=' + item));

  if (searchParas) {
    url = url + '&parameters[search]=' + searchParas;
  }
  return url;
}
